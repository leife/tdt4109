import os
import re
import sys
import copy
import inquirer
import tabulate
from textwrap import wrap
from termcolor import colored, cprint


class Game:
    def __init__(self, board, solved_board):
        """
        The function takes in a board and a solved board and sets the board to the board and the board
        copy to a deep copy of the board.

        :param board: The board that the user is currently working on
        :param solved_board: This is the solved board that we will use to compare our current board to
        """
        self.board = board
        self.__board_copy = copy.deepcopy(self.board)
        self.__solved_board = solved_board
        self.__draw_board = self.__make_board_printer()
        self.width = 60

        if os.name == 'nt':
            os.system('color')

    def __make_board_printer(self):
        bar = '-------------------------\n'
        line_format = '|' + (' {:}'*3 + ' |')*3 + '\n'
        bft = bar + (line_format*3+bar)*3
        return (lambda board: print(bft.format(*(element if element == self.__board_copy[i][j] and element != 0 else colored(str(element), 'blue') if element != 0 else "⋅" for i, row in enumerate(board) for j, element in enumerate(row)))))

    def __number_in_column(self, number, target_column):
        """
        If the number is in the column, return True, otherwise return False

        :param number: The number we're checking for
        :param target_column: The column we're checking for the number in
        :return: True or False
        """
        for row in self.board:
            if number == row[target_column]:
                return True

        return False

    def __number_in_row(self, number, target_row):
        """
        If the number is in the target row, return True, otherwise return False

        :param number: the number we're checking for
        :param target_row: The row we want to check for the number in
        :return: True or False
        """
        if number in self.board[target_row]:
            return True

        return False

    def __number_in_block(self, number, row, column):
        """
        If the row is less than 3, then check the first 3 rows and columns. If the row is less than 6,
        then check the middle 3 rows and columns. If the row is less than 9, then check the last 3 rows
        and columns

        :param number: the number we're checking
        :param row: the row of the cell you want to check
        :param column: the column of the cell you want to check
        :return: A boolean value.
        """
        if row < 3:
            if column < 3:
                for i in range(3):
                    for j in range(3):
                        if number == self.board[i][j]:
                            return True
            elif column < 6:
                for i in range(3):
                    for j in range(3, 6):
                        if number == self.board[i][j]:
                            return True
            else:
                for i in range(3):
                    for j in range(6, 9):
                        if number == self.board[i][j]:
                            return True

        elif row < 6:
            if column < 3:
                for i in range(3, 6):
                    for j in range(3):
                        if number == self.board[i][j]:
                            return True
            elif column < 6:
                for i in range(3, 6):
                    for j in range(3, 6):
                        if number == self.board[i][j]:
                            return True
            else:
                for i in range(3, 6):
                    for j in range(6, 9):
                        if number == self.board[i][j]:
                            return True
        elif row < 9:
            if column < 3:
                for i in range(6, 9):
                    for j in range(3):
                        if number == self.board[i][j]:
                            return True
            elif column < 6:
                for i in range(6, 9):
                    for j in range(3, 6):
                        if number == self.board[i][j]:
                            return True
            else:
                for i in range(6, 9):
                    for j in range(6, 9):
                        if number == self.board[i][j]:
                            return True

        return False

    def __check_if_in_original_board(self, row, column):
        """
        If the number is in the original board, return True, otherwise return False

        :param number: The number that we want to check if it's in the original board
        :param row: The row of the cell you want to check
        :param column: The column of the cell you want to check
        :return: a boolean value.
        """
        if self.__board_copy[row][column] != 0:
            return True

        return False

    def __validate_coordinate(self, row, column):
        """
        It checks if the row and column are valid coordinates

        :param row: The row of the coordinate
        :param column: The column of the cell to be validated
        :return: a boolean value.
        """
        if len(str(row) + str(column)) != 2 and not 0 <= row <= 8 and not 0 <= column <= 8:
            return False
        return True

    def __handle_input(self):
        """
        It takes a number and a coordinate, checks if the number is already in the block, row or column,
        and if not, it adds the number to the board
        :return: None
        """

        try:
            question = [
                inquirer.Text(
                    'row', message="Enter the row of the cell you want to fill in (1-9) "),
                inquirer.Text(
                    'column', message="Enter the column of the cell you want to fill in (1-9) ")
            ]

            answers = inquirer.prompt(question)
            row, column = int(answers['row'])-1, int(answers['column'])-1

            question = [
                inquirer.Text('number', message="Which number would you like to enter?"), ]

            number = int(inquirer.prompt(question)['number'])

            if number < 1 or number > 9:
                cprint("\nInvalid number, number must be between 1 and 9",
                       "red", attrs=["bold"])
                return

            if not self.__validate_coordinate(row, column):
                cprint("\nInvalid coordinate", "red", attrs=["bold"])
                return

            if self.__check_if_in_original_board(row, column):
                cprint("\nThere is already a number in that cell",
                       "red", attrs=["bold"])
                return
            elif self.__number_in_block(number, row, column):
                cprint(f"\n{number} already in block", "red", attrs=["bold"])
                return
            elif self.__number_in_row(number, row):
                cprint(f"\n{number} already in row {row+1}",
                       "red", attrs=["bold"])
                return
            elif self.__number_in_column(number, column):
                cprint(f"\n{number} already in column {column+1}",
                       "red", attrs=["bold"])
                return

            self.board[row][column] = number
            cprint(
                f"\nInserted {number} at ({column+1},{row+1})", "green", attrs=["bold"])
        except:
            cprint("\nInvalid input", "red", attrs=["bold"])
            return

    def __handle_delete(self):
        """
        It takes a coordinate from the user, checks if it's valid, and if it is, it deletes the number
        at that coordinate
        :return: None
        """

        try:
            question = [
                inquirer.Text(
                    'row', message="Enter the row of the cell you want to fill in (1-9) "),
                inquirer.Text(
                    'column', message="Enter the column of the cell you want to fill in (1-9) ")
            ]

            answers = inquirer.prompt(question)
            row, column = int(answers['row'])-1, int(answers['column'])-1

            if not self.__validate_coordinate(row, column):
                cprint("\nInvalid coordinate", "red", attrs=["bold"])
                return
            elif self.__check_if_in_original_board(row, column):
                cprint(
                    "\nYou can't delete a number that is in the original board", "red", attrs=["bold"])
                return

            cprint(
                f"\nDeleted {self.board[row][column]} at ({column+1},{row+1})", "green", attrs=["bold"])
            self.board[row][column] = 0
        except:
            cprint("\nInvalid coordinate", "red", attrs=["bold"])
            return

    def __check_win(self):
        """
        If the board is equal to the solved board, return True. Otherwise, return False
        :return: If the board is solved or not.
        """
        if self.board == self.__solved_board:
            return True

        return False

    def __board_to_code(self, input_board=None):
        """
        It takes a 2D list of numbers and turns it into a string of numbers

        :param input_board: the board you want to turn into a code. If you don't specify one, it will
        use the board that the object was initialized with
        :return: The code is being returned.
        """
        if input_board:
            _code = ''.join([str(i) for j in input_board for i in j])
            return _code
        else:
            self.code = ''.join([str(i) for j in self.board for i in j])
            return self.code

    def __save_board(self, filename):
        """
        It takes the board and converts it to a string, then saves that string to a file

        :param filename: The name of the file to save the board to.
        (optional)
        """
        board_code = self.__board_to_code(self.board)

        with open(filename, "w") as f:
            f.write(board_code)
            cprint(f"\nFile saved as {filename}", "green", attrs=["bold"])

    def __print_instructions(self):
        """
        It prints the instructions for the game
        """
        line = "+-" + "-" * self.width + "-+"
        cprint(line, "white")
        for text_line in wrap(tabulate.tabulate(
                ["WELCOME TO SUDOKU!"], tablefmt="plain"), self.width):
            cprint('| {0:^{1}} |'.format(text_line, self.width), "white")
        cprint(line, "white")

    def play(self):
        """
        The function prints instructions, then asks the user if they want to play. If they do, it draws
        the board, then asks for a command. If the command is "i", it calls the function to handle
        input. If the command is "d", it calls the function to handle deletion. If the command is "s",
        it asks for a filename, then calls the function to save the board. If the command is "q", it
        quits. If the command is none of the above, it prints an error message. If the user wins, it
        prints a message and quits
        :return: The board is being returned.
        """
        try:
            self.__print_instructions()
            playing = True if input(
                colored("Press enter to play: ", "green", attrs=["bold", "blink"])) == "" else False
            print()

            if not playing:
                cprint("Exiting...", "red", attrs=["bold"])
                return

            while playing:
                self.__draw_board(self.board)

                question = [
                    inquirer.List('command', message="What would you like to do? ", choices=[
                                  'input', 'delete', 'save', 'quit'], default='input')
                ]
                command = inquirer.prompt(question)['command']

                if command == "input":
                    self.__handle_input()
                elif command == "delete":
                    self.__handle_delete()
                elif command == "save":
                    question = [
                        inquirer.Text('filename', message="What is the name of the file? ", validate=lambda _, x: re.match("^[\w\-. ]+$", x), default="sudoku_board.txt")]
                    filename = inquirer.prompt(question)['filename']
                    if len(filename.strip()) != 0:
                        self.__save_board(self.__board_to_code(filename))
                    else:
                        cprint("\nFilename cannot be empty",
                               "red", attrs=["bold"])
                    playing = False
                elif command == "quit":
                    cprint("\nExiting...", "red", attrs=["bold"])
                    playing = False
                else:
                    cprint("\nInvalid command", "red", attrs=["bold"])

                if self.__check_win():
                    line = "+-" + "-" * self.width + "-+"
                    cprint(line, "green")
                    for text_line in wrap(tabulate.tabulate(
                            ["YOU WIN!"], tablefmt="plain"), self.width):
                        cprint('| {0:^{1}} |'.format(
                            text_line, self.width), "green")
                    cprint(line, "green")
                    self.__draw_board(self.board)
                    playing = False

        except KeyboardInterrupt:
            print("\nExiting...")
            sys.exit(0)
