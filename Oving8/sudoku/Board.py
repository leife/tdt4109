import copy
import random


class Board:
    def __init__(self, code=None):
        """
        The function takes a string of 81 digits and uses it to populate the board

        :param code: The code for the board. If this is not provided, the board will be empty
        """
        self.__reset_board()

        if code:
            self.code = code

            for row in range(9):
                for column in range(9):
                    self.board[row][column] = int(code[0])
                    code = code[1:]
        else:
            self.code = None

    def __reset_board(self):
        """
        This function resets the board to a blank board
        :return: The board is being returned.
        """
        self.board = [[0 for _ in range(9)] for _ in range(9)]
        return self.board

    def board_to_code(self, input_board=None):
        """
        It takes a 2D list of numbers and turns it into a string of numbers

        :param input_board: the board you want to turn into a code. If you don't specify one, it will
        use the board that the object was initialized with
        :return: The code is being returned.
        """
        if input_board:
            _code = ''.join([str(i) for j in input_board for i in j])
            return _code
        else:
            self.code = ''.join([str(i) for j in self.board for i in j])
            return self.code

    def code_to_board(self, input_code):
        return [[int(number) for number in input_code[i:i+9]]
                for i in range(0, len(input_code), 9)]

    def find_spaces(self, input_board=None):
        """
        It returns the first space it finds on the board
        :return: The row and column of the first space found in the board.
        """
        if input_board is None:
            input_board = self.board

        for row in range(len(input_board)):
            for column in range(len(input_board[0])):
                if input_board[row][column] == 0:
                    return (row, column)

        return False

    def check_space(self, num, space):
        """
        If the space is empty, check if the number is in the row, column, or internal box

        :param num: The number we're trying to place in the space
        :param space: The space we're checking
        :return: A boolean value.
        """
        if not self.board[space[0]][space[1]] == 0:
            return False

        for column in self.board[space[0]]:
            if column == num:
                return False

        for row in range(len(self.board)):
            if self.board[row][space[1]] == num:
                return False

        _internal_box_row = space[0] // 3
        _internal_box_column = space[1] // 3

        for i in range(3):
            for j in range(3):
                if self.board[i + (_internal_box_row * 3)][j + (_internal_box_column * 3)] == num:
                    return False

        return True

    def solve(self):
        """
        If there are no spaces available, return True. Otherwise, for each number from 1 to 9, if the
        number is valid in the space, set the space to the number, and if the board is solvable, return
        the board. If the board is not solvable, set the space back to 0
        :return: The board with the solved puzzle.
        """
        _spaces_available = self.find_spaces()

        if not _spaces_available:
            return True
        else:
            row, column = _spaces_available

        for number in range(1, 10):
            if self.check_space(number, (row, column)):
                self.board[row][column] = number

                if self.solve():
                    return self.board

                self.board[row][column] = 0

        return False

    def solve_for_code(self):
        """
        It takes a board, solves the board, converts the solution to a code,
        and returns the code
        :return: The solved code string is being returned.
        """
        return self.board_to_code(self.solve())

    def __generate_random_complete_board(self):
        """
        It generates a random complete board, and then generates a random incomplete board from it
        :return: The board is being returned.
        """
        self.__reset_board()

        _available_numbers = list(range(1, 10))
        for row in range(3):
            for column in range(3):
                _num = random.choice(_available_numbers)
                self.board[row][column] = _num
                _available_numbers.remove(_num)

        _available_numbers = list(range(1, 10))
        for row in range(3, 6):
            for column in range(3, 6):
                _num = random.choice(_available_numbers)
                self.board[row][column] = _num
                _available_numbers.remove(_num)

        _available_numbers = list(range(1, 10))
        for row in range(6, 9):
            for column in range(6, 9):
                _num = random.choice(_available_numbers)
                self.board[row][column] = _num
                _available_numbers.remove(_num)

        return self.__generate_count()

    def __generate_count(self):
        """
        If the space is empty, generate a random number, check if it's valid, if it is, set the space to
        that number, and then recursively call the function again. If it's not valid, set the space back
        to 0
        :return: The board is being returned.
        """
        for row in range(len(self.board)):
            for column in range(len(self.board[row])):
                if self.board[row][column] == 0:
                    _num = random.randint(1, 9)

                    if self.check_space(_num, (row, column)):
                        self.board[row][column] = _num

                        if self.solve():
                            self.__generate_count()
                            return self.board

                        self.board[row][column] = 0

        return False

    def __solve_to_find_number_of_solutions(self, row, column):
        """
        If the number we're trying to place in the current space is valid, place it there and try to
        solve the board. If we can solve the board, return the solution. If we can't solve the board,
        reset the space to 0 and try the next number

        :param row: The row of the space we're trying to fill
        :param column: The column of the space we're checking
        :return: The board is being returned.
        """
        for number in range(1, 10):
            if self.check_space(number, (row, column)):
                self.board[row][column] = number

                if self.solve():
                    return self.board

                self.board[row][column] = 0

        return False

    def __find_spaces_to_find_number_of_solutions(self, board, h):
        """
        It returns the row and column of the hth empty space on the board

        :param board: The board that we're trying to solve
        :param h: The number of the solution you want to find
        :return: The row and column of the space that is being filled in.
        """
        _k = 1
        for row in range(len(board)):
            for col in range(len(board[row])):
                if board[row][col] == 0:
                    if _k == h:
                        return (row, col)

                    _k += 1

        return False

    def find_number_of_solutions(self):
        """
        It creates a copy of the board, and then solves the board by filling in the first empty space
        with a 1, then the second empty space with a 1, then the third empty space with a 1, etc. until
        it has filled in all empty spaces with a 1. 

        It then takes the solution to that board and appends it to a list. 

        It then repeats the process, but this time filling in the first empty space with a 2, then the
        second empty space with a 2, then the third empty space with a 2, etc. until it has filled in
        all empty spaces with a 2. 

        It then takes the solution to that board and appends it to a list. 

        It then repeats the process, but this time filling in the first empty space with a 3, then the
        second empty space with a 3, then the third empty space with a 3, etc. until it has filled in
        all empty
        :return: A list of all possible solutions to the board.
        """
        _zeros = 0
        _list_of_solutions = []

        for row in range(len(self.board)):
            for col in range(len(self.board[row])):
                if self.board[row][col] == 0:
                    _zeros += 1

        for i in range(1, _zeros+1):
            _board_copy = copy.deepcopy(self)

            _row, _col = self.__find_spaces_to_find_number_of_solutions(
                _board_copy.board, i)
            _board_copy_solution = _board_copy.__solve_to_find_number_of_solutions(
                _row, _col)

            _list_of_solutions.append(self.board_to_code(
                input_board=_board_copy_solution))

        return list(set(_list_of_solutions))

    def generate_sudoku_board(self, full_board, difficulty):
        """
        It removes random squares from the board until there is only one solution

        :param full_board: The full board, with all the numbers filled in
        :param difficulty: 0 = easy, 1 = medium, 2 = hard
        :return: The board and the full board.
        """
        self.board = copy.deepcopy(full_board)

        if difficulty == 0:
            _squares_to_remove = 36
        elif difficulty == 1:
            _squares_to_remove = 46
        elif difficulty == 2:
            _squares_to_remove = 52
        else:
            return

        _counter = 0
        while _counter < 4:
            _random_row = random.randint(0, 2)
            _random_column = random.randint(0, 2)
            if self.board[_random_row][_random_column] != 0:
                self.board[_random_row][_random_column] = 0
                _counter += 1

        _counter = 0
        while _counter < 4:
            _random_row = random.randint(3, 5)
            _random_column = random.randint(3, 5)
            if self.board[_random_row][_random_column] != 0:
                self.board[_random_row][_random_column] = 0
                _counter += 1

        _counter = 0
        while _counter < 4:
            _random_row = random.randint(6, 8)
            _random_column = random.randint(6, 8)
            if self.board[_random_row][_random_column] != 0:
                self.board[_random_row][_random_column] = 0
                _counter += 1

        _squares_to_remove -= 12
        _counter = 0
        while _counter < _squares_to_remove:
            _row = random.randint(0, 8)
            _column = random.randint(0, 8)

            if self.board[_row][_column] != 0:
                number = self.board[_row][_column]
                self.board[_row][_column] = 0

                if len(self.find_number_of_solutions()) != 1:
                    self.board[_row][_column] = number
                    continue

                _counter += 1

        return self.board, full_board

    def genereate_question_board_code(self, difficulty):
        """
        It generates a random complete board, then generates a question board from it, and returns the
        question board and the solution board

        :param difficulty: The difficulty of the board
        :return: The board and the solution board are being returned.
        """
        self.board, _solution_board = self.generate_sudoku_board(
            self.__generate_random_complete_board(), difficulty)
        return self.board_to_code(), self.board_to_code(_solution_board)
