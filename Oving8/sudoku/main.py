from Game import Game
from Board import Board
from functions import setup
from termcolor import cprint


def main():
    """
    `main()` creates a `Board` object, calls `setup()` to create an unsolved and solved board, and then
    creates a `Game` object and calls its `play()` method.
    """
    board = Board()
    try:
        unsolved_board, solved_board = setup(board)
    except TypeError:
        cprint("Error while loading board", "red")
        return
    game = Game(unsolved_board, solved_board)
    game.play()


if __name__ == "__main__":
    main()
