import os
import re
import sys
import copy
import inquirer
from halo import Halo
from Board import Board
from termcolor import cprint


def setup(board):
    """
    It asks the user if they want to load a board from a file, and if they do, it loads the board from
    the file, solves it, and returns the unsolved and solved boards. If they don't want to load a board
    from a file, it asks the user for a difficulty level, generates a board, solves it, and returns the
    unsolved and solved boards

    :param board: the board object
    :return: unsolved_board, solved_board
    """

    question = [
        inquirer.List('have_board_code', message="Do you have a board code?", choices=["No", "Yes"], default="No")]

    generate_board_from_code = inquirer.prompt(question)['have_board_code']

    if generate_board_from_code[0] == "Y":
        try:
            question = [
                inquirer.Text('filename', message="What is the name of the file? ", validate=lambda _, x: re.match("^[\w\-. ]+$", x), default="sudoku_board.txt")]
            filename = inquirer.prompt(question)['filename']
            try:
                with open(filename, "r") as f:
                    board_code = f.read().strip()
            except:
                cprint("File not found", "red")
                sys.exit(1)
            new_board = Board(board_code)
            try:
                with Halo(text='Solving...', spinner='dots'):
                    solved_board_code = new_board.solve_for_code()
            except TypeError:
                cprint("Board already solved!", "red")
                sys.exit(1)
            if not 'solved_board_code' in locals():
                solved_board_code = copy.deepcopy(board_code)
            unsolved_board = new_board.code_to_board(board_code)
            solved_board = new_board.code_to_board(solved_board_code)
            os.remove(filename)
        except:
            cprint("An error occurred while loading the board", "red")
            sys.exit(1)
    elif generate_board_from_code[0] == "N":
        try:
            questions = [
                inquirer.List('difficulty',
                              message="Which difficulty level do you want?",
                              choices=['Easy', 'Medium', 'Hard'],
                              ),
            ]

            difficulty = inquirer.prompt(questions)['difficulty']
        except ValueError:
            print("Invalid difficulty!")
            sys.exit(1)

        map_difficulty = {"Easy": 0, "Medium": 1, "Hard": 2}
        with Halo(text='Loading', spinner='dots'):
            board_codes = board.genereate_question_board_code(
                map_difficulty[difficulty])
        unsolved_board = board.code_to_board(board_codes[0])
        solved_board = board.code_to_board(board_codes[1])
    else:
        cprint("Invalid input", "red")
        sys.exit(1)

    os.system("clear")
    return unsolved_board, solved_board
